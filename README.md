# Portafolio
Repositorio donde se encontrara una muestra de algunos trabajos realizados en las distintas plataformas.
<br>
<hr />
 <table border="2px">
  <tr>
    <td>
      <h1> -------- < Android > -------- </h1>
        <b><h2>Proyecto Telefonica</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Android-Nativo<br><br>
        <b>Servicios  :</b> Web Service <br><br>
        <b>Descripcion : </b> Aplicación encargada de brindar soporte, (FAQ), entre otros<br>
        a los usuarios de la compañia Telefonica, brindado la opcion de<br> 
        compartir los distintos articulos en las redes sociales de preferencia.
    </td>
    <td><img src="Gif_project/movistar.gif"></td>
  </tr>
  <tr>
    <td>
        <b><h2>Proyecto Pasify</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Android-Nativo<br><br>
        <b>Servicios  :</b> Web Service, Base de datos, Login social, Google Map, Google Directions, GPS, FCM <br><br>
        <b>Descripcion : </b> Aplicación encargada de trazar rutas mas optimas de un viaje<br>
        usando el gps del usuario, para indicar datos como velocidad<br> 
        y posición en tiempo real.
    </td>
    <td><img src="Gif_project/pasify.gif"></td>
  </tr>
  <tr>
    <td>
        <b><h2>Proyecto SwitchMeditation</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Android-Nativo<br><br>
        <b>Servicios  :</b> Material Desing, Reproductor archivos mp3 <br><br>
        <b>Descripcion : </b> Aplicación para meditaciones a travez del metodo switch<br>
        el cual a travez de meditaciones guiadas puede mejorar el ritmo de<br> 
        vida y estado de animo de una persona.
    </td>
    <td><img src="Gif_project/swichMeditation.gif"></td>
  </tr>
  <tr>
    <td>
        <b><h2>Proyecto Kresko</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Android-Nativo<br><br>
        <b>Servicios  :</b> Material Desing, Web Services, FCM, Organizador Personal <br><br>
        <b>Descripcion : </b> Aplicación encargada de organizar la agenda de un<br>
        coworker, brinda capacidad de reserva de espacios, funcionalidades basicas<br> 
        de red social y control de actividades en agenda.
    </td>
    <td><img src="Gif_project/kresko.gif"></td>
  </tr>
  <tr>
    <td>
        <b><h2>Proyecto Glise</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Android-Nativo<br><br>
        <b>Servicios  :</b> Web Service, Base de datos, Lector codigos QR, Google Map<br><br>
        <b>Descripcion : </b> Aplicacion encargada de llevar un control de compra<br> 
        entre los productos que se deseaban, graficando en un mapa los supermecados<br>
        mas cercanos y colocando los precios que existia en el establecimiento<br>
        tambien si algun producto poseia ofertas o rebajas.
    </td>
    <td><img src="Gif_project/glise.gif"></td>
  </tr>
  <tr>
    <td>
        <b><h2>Proyecto Appizard</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Android-Nativo<br><br>
        <b>Servicios  :</b> Social Login, Web Service, Base de datos, Cloud Messaging <br><br>
        <b>Descripcion : </b> Aplicacion encargada de mostrar y manejar noticias en el area<br>
        de informatica en la actualidad dando a los usuarios capacidad de compartirlas <br>
        guardarlas etc, tambien permitia enviar correos electronicos para <br>
        responder dudas o pedir informacion sobre el costro de crear una app o una sitio web 
    </td>
    <td><img src="Gif_project/appizard.gif"></td>
  </tr>
   <tr>
    <td>
        <b><h2>Proyecto Geneica</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Android-Nativo<br><br>
        <b>Servicios  :</b> Social Login, Cloud Messaging,  Web Service, Base de datos, Cache service<br><br>
        <b>Descripcion : </b> Aplicación encargada de manejar un servicio de catering llevando<br>
        las cuentas por clientes de los productos y servicios solicitados<br>
        a la empresa, manejando costos enviando cotizaciones y manteniendo la<br> 
        informacion actual tanto del usuario como del servicio prestado
    </td>
    <td><img src="Gif_project/geneica.gif"></td>
  </tr>
   <tr>
    <td>
      <h1> -------- < Web > -------- </h1>
        <b><h2>Que hacer en Internet</h2></b><br>
        <b>Lenguaje Desarrollado :</b> HTML5, CSS3 y PHP<br><br>
        <b>Servicios  :</b> Social Login, Web Service, Base de datos<br><br>
        <b>Descripcion : </b> Pagina web encargada de mostrar variados tipos de temas<br>
        multimedias al usuario de acuerdo a su estado de animo verificando en <br>
        una base de datos de acuerdo a sus gustos y el de otros usuarios cuales<br> 
        son los temas de mayor interes de acuerdo a su estado de humor.
    </td>
    <td><img src="Gif_project/quehacerinternet.gif"></td>
  </tr>
   <tr>
    <td>
        <b><h2>Tor-Elecs Mileniun </h2></b><br>
        <b>Lenguaje Desarrollado :</b> HTML5, CSS3 y PHP<br><br>
        <b>Servicios  :</b> Login, Web Service, Base de datos, Administracion<br><br>
        <b>Descripcion : </b> E-commerce de una venta de repuestos automotriz <br>
        encargado de llevar inventario, compras, clientes, cuentas por pagar, deudas <br>
        y demas operaciones administrativas para un sistema de inventario ademas <br> 
        de la capacidad de realizar ventas a travez de la web.
    </td>
    <td><img src="Gif_project/torelecs.gif"></td>
  </tr>
     <tr>
    <td>
        <b><h2>Ice Cream </h2></b><br>
        <b>Lenguaje Desarrollado :</b> HTML5, CSS3<br><br>
        <b>Descripcion : </b> Pagina de referencia para probar algunos <br>
        modelos de diseño, patroces css3 y html5 usado bajo los estandares<br>
        correcto segun indica la web.
    </td>
    <td><img src="Gif_project/heladeria.gif"></td>
  </tr>
  </tr>
  <tr>
    <td>
      <h1> -------- < Robotica > -------- </h1>
        <b><h2>Hexapode</h2></b><br>
        <b>Lenguaje Desarrollado :</b> C, Pinguino-IDE<br><br>
        <b>Descripcion : </b> Prototipo de robot hexapodo creado para <br>
        estudiar el movimiento de un robot hexapodo y potenciar mis habilidades<br>
        tanto mecanicas como electronicas, a travez de la creacion de un <br>
        mecanismo capaz de moverse por diferentes tipos de terreno, al mismo<br>
        tiempo adaptar una serie de sensores para que fuera capaz de<br>
        tomar sus propias decisiones con la ayuda de un microcontrolador.
    </td>
    <td><img src="Gif_project/hexapodo.gif"></td>
  </tr>
  <tr>
    <td>
        <b><h2>Bigfonn</h2></b><br>
        <b>Lenguaje Desarrollado :</b> C, Pinguino-IDE<br><br>
        <b>Descripcion : </b> Prototipo de robot buscador de camino <br>
        que esta programado para esquivar cualquier obstaculo que se  <br>
        interponga en su camino, guardar los recorridos realizados y <br>
        ser capaz de tomar decisiones sobre cual ruta es la mas optima<br>
        de manera automatica.
    </td>
    <td><img src="Gif_project/bigfonn.gif"></td>
  </tr>
  <tr>
    <td>
        <b><h2>Bebot</h2></b><br>
        <b>Lenguaje Desarrollado :</b> C, Pinguino-IDE<br><br>
        <b>Descripcion : </b> Prototipo de robot musical, en esencia <br>
         Bebot no es un robot, pero si una caja musical, la misma fue<br>
        realizada para colocar a prueba las capacidades del  <br>
        microcontrolador y ciertos circuitos electronicos para lograr<br>
        reproducir musica en formato mp3, leer/almacenar informacion <br>
        en una tarjeta micro SD  a travez de un microcontrolador y <br>
        estudiar un poco sobre los factores que afectan en la acustica <br>
        e impedancia, de esta manera logre crear un equipo de reproduccion<br>
        de mp3 de bajo costo y con buen aspecto. <br>
    </td>
    <td><img src="Gif_project/bebot.gif"></td>
  </tr>
   <tr>
    <td>
      <h1> -------- < Realidad Aumentada > -------- </h1>
        <b><h2>Dragon</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Unity<br><br>
        <b>Descripcion : </b> Prueba realizada usando el SDK de Realidad Aumentada<br>
        de Vuforia y usando el motor grafico Unity para programar el <br>
        personaje, consiste en la lectura y reconocimiento de un patron en la<br>
        pantalla, una vez realizado esto se procede a superponer una imagen<br>
        y un objeto 3D animado.<br>
    </td>
    <td><img src="Gif_project/dragon.gif"></td>
  </tr>
   <tr>
    <td>
      <h1> -------- < Realidad Virtual > -------- </h1>
        <b><h2>Boss</h2></b><br>
        <b>Lenguaje Desarrollado :</b> Unity, Android<br><br>
        <b>Descripcion : </b> Prueba realizada usando el SDK de Realidad Virtual <br>
        de Google Cardboard y usando el motor grafico Unity para programar<br>
        una serie de personajes y movimientos del dispositivo con el fin<br>
        de crear un entorno virtual que brinde total inmersion al usuario.<br>
    </td>
    <td><img src="Gif_project/Boss.gif"></td>
  </tr>
</table>


